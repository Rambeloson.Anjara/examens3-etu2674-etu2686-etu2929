<?php
function checkConnexion($login,$password)
{
    $query=sprintf("select id from the_membres where email='%s' and motdepasse='%s'",$login,$password);
    $result=mysqli_query(dbconnect(),$query);
    $user=mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    if($user!=null)
    {
        return null;
    }
    return $user;
}
function getVarietesThes()
{
    $query = "select * from the_varietes";
    $result = mysqli_query(dbconnect(), $query);
    $listeVarietes = array();
    while ($variete = mysqli_fetch_assoc($result)) {
        $listeVarietes[] = array(
            'id' => $variete['id'],
            'nom' => $variete['nom'],
            'occupation' => $variete['occupation'],
            'rendement' => $variete['rendement'],
        );
    }

    mysqli_free_result($result);
    return $listeVarietes;
}
function updateSalaire($montant)
{
    $query = sprintf("INSERT INTO the_Salaires (dateSalaire, montant) VALUES (NOW(), '%s')",$montant);
    $result = mysqli_query(dbconnect(), $query);

    if ($result) {
        return true;
    } else {
        return false;
    }
}
function insertNewParcelle($idThe,$surface)
{
    $query = sprintf("INSERT INTO the_Parcelles (idThe,surface) VALUES ('%s','%s')",$idThe,$surface);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}
function insertNewCueillette($date,$idCueilleur,$idParcelle,$poids)
{
    $query = sprintf("INSERT INTO the_cueillettes(dateCueillette,idCueilleur,idParcelle,poids) VALUES ('%s','%s','%s','%s')",$date,$idCueilleur,$idParcelle,$poids);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}
function getPoidsTotalCueilletteParCueilleur($idCueilleur)
{
    $query = sprintf("SELECT SUM(poids) as poidsTotal FROM the_cueillettes WHERE idCueilleur='%s'",$idCueilleur);
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}
function getPoidsTotalCueilletteParCueilleurParMois($idCueilleur,$mois)
{
    $query = sprintf("SELECT SUM(poids) as poidsTotal FROM the_cueillettes WHERE idCueilleur='%s' AND MONTH(dateCueillette)='%s'",$idCueilleur,$mois);
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}
function getPoidsTotalCueilletteParParcelle($idParcelle)
{
    $query = sprintf("SELECT SUM(poids) as poidsTotal FROM the_cueillettes WHERE idParcelle='%s'",$idParcelle);
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}
function getPoidsTotalCueillette()
{
    $query = "SELECT SUM(poids) as poidsTotal FROM the_cueillettes";
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}
function rendementMoyen()
{
    $query = "SELECT AVG(poids) as rendementMoyen FROM the_cueillettes";
    $result = mysqli_query(dbconnect(), $query);
    $rendementMoyen = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $rendementMoyen;
}
function calculRevientParPeriodeParKilo($dateDebut,$dateFin)
{
    
}
function totalSalairesPayes($dateDebut,$dateFin)
{
    
}
?>