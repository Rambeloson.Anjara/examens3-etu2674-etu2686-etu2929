<?php
include("dbconnect.php");
date_default_timezone_set('Indian/Antananarivo');
function checkConnexion($login,$password)
{
    $query=sprintf("select id from the_Membres where email='%s' and motdepasse='%s'",$login,$password);
    $result=mysqli_query(dbconnect(),$query);
    $user=mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $user;
}

function check_admin($idUtilisateur)
{
    $requete = "SELECT is_admin FROM the_Membres WHERE id = %s";
    $requete=sprintf($requete, $idUtilisateur );
    $resultat = mysqli_query(dbconnect(),$requete);
    $ligne = mysqli_fetch_assoc($resultat);
    if($ligne == NULL)
    {
        return false;
    }
    else
    {
        if  ($ligne['is_admin']=="1")
        {
            return true;
        }
       return false;
    }  
}

function getVarietesThes()
{
        $requete = sprintf("SELECT * from the_Thes");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $listeThes = array();
        while ($thes = mysqli_fetch_assoc($resultat)) {
            $listeThes[] = $thes;
        }
        mysqli_free_result($resultat);
        return json_encode($listeThes);
}

function insertNewParcelle($idThe,$surface)
{
    $query = sprintf("INSERT INTO the_Parcelles (idThe,surface) VALUES ('%s','%s')",$idThe,$surface);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getParcelles()
{
        $requete = sprintf("SELECT p.* , t.nom from the_Parcelles AS p join the_Thes as t on p.idThe=t.id" );
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $listeParcelles = array();
        while ($parcelles = mysqli_fetch_assoc($resultat)) {
            $listeParcelles[] = $parcelles;
        }
        mysqli_free_result($resultat);
        return $listeParcelles; 
}

function getCueilleurs()
{
        $requete = sprintf("SELECT * from the_Cueilleurs");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $listeCueilleurs = array();
        while ($cueilleurs = mysqli_fetch_assoc($resultat)) {
            $listeCueilleurs[] = $cueilleurs;
        }
        mysqli_free_result($resultat);
        return $listeCueilleurs; 
}

function updateSalaire($montant)
{
    $query = sprintf("INSERT INTO the_Salaires (dateSalaire, montant) VALUES (NOW(), '%s')",$montant);
    $result = mysqli_query(dbconnect(), $query);

    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSalairesActuel()
{
    $requete = sprintf("SELECT * FROM the_Salaires ORDER BY dateSalaire DESC LIMIT 1");
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $dernierSalaire = mysqli_fetch_assoc($resultat);
    mysqli_free_result($resultat);
    return $dernierSalaire; 
}
function getCategorieDepenses()
{
        $requete = sprintf("SELECT * from the_CategorieDepenses");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $listeCategorieDepenses = array();
        while ($categorieDepenses= mysqli_fetch_assoc($resultat)) {
            $listeCategorieDepenses[] = $categorieDepenses;
        }
        mysqli_free_result($resultat);
        return $listeCategorieDepenses; 
}

function insertNewCueilleurs($nom,$genre,$dtn)
{
    $requete="INSERT into the_Cueilleurs values (null,'%s', '%s', '%s')";
    $requete=sprintf($requete, $nom,$genre,$dtn);
    $result = mysqli_query(dbconnect(), $requete);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function insertNewCategorieDepenses($nom)
{
    $requete="INSERT into the_CategorieDepenses values (null,'%s')";
    $requete=sprintf($requete, $nom);
    $result = mysqli_query(dbconnect(), $requete);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function insertNewVariete($nom,$occupation,$rendement,$prix)
{
    $requete="INSERT into the_Thes values (null,'%s', '%s', '%s','%s')";
    $query=sprintf($requete,$nom,$occupation,$rendement,$prix);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
    
}

function insertNewDepenses($idCD,$date,$montant,$idMembres)
{ 
    if($montant<0)
    {
        return false;
    }
    $requete="INSERT into the_Depenses values (null,'%s', '%s', '%s','%s')";
    $requete=sprintf($requete,$idCD,$date,$montant,$idMembres);
    $result = mysqli_query(dbconnect(), $requete);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function updateTable($id,$table,$colonne,$nouvelleValeur)
{
    $requete="UPDATE %s SET %s = %s WHERE id=%s";
    $requete=sprintf($requete, $table, $colonne, $nouvelleValeur,$id);
    mysqli_query( dbconnect(),$requete);
}
function updateCategorieDepenses($id,$nom)
{
    $requete="UPDATE the_CategorieDepenses SET nom = %s WHERE id=%s";
    $requete=sprintf($requete,$nouvelleValeur,$id);
    mysqli_query( dbconnect(),$requete);
}

function updateCueilleur($id,$nom,$genre,$dtn)
{
    $requete="UPDATE the_Cueilleurs SET nom = %s,genre=%S, dtn=%s  WHERE id=%s";
    $requete=sprintf($requete,$nom,$genre,$dtn,$id);
    mysqli_query( dbconnect(),$requete);
}

function updateParcelle($id,$surface,$the)
{
    $requete="UPDATE the_Parcelles SET surface = %s, idThe=%S WHERE id=%s";
    $requete=sprintf($requete,$surface,$the,$id);
    mysqli_query( dbconnect(),$requete);
}
function updateThe( $variete,$id,$occupation,$rendement)
{
    $requete="UPDATE the_Thes SET nom= %s, occupation=%S, rendement=%s WHERE id=%s";
    $requete=sprintf($requete,$variete,$occupation,$rendement,$id);
    mysqli_query( dbconnect(),$requete);
}
function supprimer($id,$table)
{
    $requete="DELETE from %s WHERE id=%s";
    $requete=sprintf($requete, $table,$id);
    mysqli_query( dbconnect(),$requete);
}

function poidsRestant($idParcelle) {
    
    $requete = sprintf("SELECT poids_restant FROM v_PoidsRestants WHERE idParcelle = %d", $idParcelle);
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $poidsRestant = $row['poids_restant'];
    mysqli_free_result($resultat);
    return $poidsRestant;
}
function checkPoidsRestant($idParcelle, $poids)
{
    $floatpoids = floatval($poids);
    $floatpoidsRestant= floatval(PoidsRestant($idParcelle));
    if ($floatpoids > $floatpoidsRestant)
    {
        return false;
    }
    return true;
}

function insertNewCueillette($date,$idCueilleur,$idParcelle,$poids)
{
    $query = sprintf("INSERT INTO the_cueillettes(dateCueillette,idCueilleur,idParcelle,poids) VALUES ('%s','%s','%s','%s')",$date,$idCueilleur,$idParcelle,$poids);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getPoidsTotalCueilletteParCueilleurParMois($idCueilleur,$mois)
{
    $query = sprintf("SELECT SUM(poids) as poidsTotal FROM the_cueillettes WHERE idCueilleur='%s' AND MONTH(dateCueillette)='%s'",$idCueilleur,$mois);
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}

function getPoidsTotalCueilletteParParcelle($idParcelle)
{
    $query = sprintf("SELECT SUM(poids) as poidsTotal FROM the_cueillettes WHERE idParcelle='%s'",$idParcelle);
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}

function getPoidsTotalCueillette()
{
    $query = "SELECT SUM(poids) as poidsTotal FROM the_cueillettes";
    $result = mysqli_query(dbconnect(), $query);
    $poidsTotal = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $poidsTotal;
}

function rendementMoyen()
{
    $query = "SELECT AVG(poids) as rendementMoyen FROM the_cueillettes";
    $result = mysqli_query(dbconnect(), $query);
    $rendementMoyen = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $rendementMoyen;
}
function getPoidsTotalCueilletteParPeriode($dateDebut, $dateFin) {
    $requete = sprintf("SELECT SUM(poids) AS poids_total FROM the_Cueillettes WHERE dateCueillette BETWEEN '%s' AND '%s'",
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $poidsTotal = $row['poids_total'];
    mysqli_free_result($resultat);
    echo print_r("poids total:".$poidsTotal);
    return $poidsTotal;
}
function getPoidsRestantParcelleGlobal($dateFin)
{
    $anneeFin = date('Y', strtotime($dateFin));
    $dateDebut = date("$anneeFin-m-01", strtotime($dateFin));
    $requete = sprintf("SELECT poids_restant FROM v_PoidsRestants WHERE dateCueillette BETWEEN '%s' AND '%s'",
                        $dateDebut,
                        mysqli_real_escape_string(dbconnect(), $dateFin));
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $poidsRestant = $row['poids_restant'];
    mysqli_free_result($resultat);
    return $poidsRestant;
}



function getPoidsRestantParcelle($idParcelle, $dateFin) {
    $anneeFin = date('Y', strtotime($dateFin));
    $dateDebut = date("$anneeFin-m-01", strtotime($dateFin));
    $requete = sprintf("SELECT poids_restant FROM v_PoidsRestants WHERE idParcelle = %d AND dateCueillette BETWEEN '%s' AND '%s'",
                        $idParcelle,
                        $dateDebut,
                        mysqli_real_escape_string(dbconnect(), $dateFin));
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $poidsRestant = $row['poids_restant'];
    mysqli_free_result($resultat);
    return $poidsRestant;
}

function getMontantSalaireEtPoidsParPeriode($dateDebut, $dateFin) {
    $requete = sprintf("SELECT 
                            SUM(montant_salaire) AS montant_total,
                            SUM(poids) AS poids_total
                        FROM 
                            v_CueilletteSalaire
                        WHERE 
                            dateCueillette BETWEEN '%s' AND '%s'",
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));

    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    mysqli_free_result($resultat);
    return $row;
}

function getTotalDepensesPeriode($dateDebut, $dateFin) {
    $requete = sprintf("SELECT SUM(montant) AS total_depenses FROM the_Depenses WHERE dateDepense BETWEEN '%s' AND '%s'",
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $totalDepenses = $row['total_depenses'];

    mysqli_free_result($resultat);
    echo print_r("depenses totales:".$totalDepenses);
    return $totalDepenses;
}

function calculRevientParKiloParPeriode($dateDebut, $dateFin)
{
     $totalDepenses = getTotalDepensesPeriode($dateDebut, $dateFin);
     $resultats = getMontantSalaireEtPoidsParPeriode($dateDebut, $dateFin);
     $montantTotal = $resultats['montant_total'];
     $poidsTotal = $resultats['poids_total'];
     if ($poidsTotal != 0) {
         $coutRevientParKilo = ($totalDepenses + $montantTotal) / $poidsTotal;
     } else {
         $coutRevientParKilo = 0; 
     }
     return $coutRevientParKilo;
}

function getTotalDepensesMois($mois) {
    $requete = sprintf("SELECT SUM(montant) AS total_depenses FROM the_Depenses WHERE MONTH(dateDepense) = %d", $mois);
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }

    $row = mysqli_fetch_assoc($resultat);
    $totalDepenses = $row['total_depenses'];

    mysqli_free_result($resultat);
    return $totalDepenses;
}
function updatePoidsMinimal($valeur)
{
    $requete="INSERT into the_PoidsMinimal values (null,'%s', NOW())";
    $query=sprintf($requete,$valeur);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function updateBonusCueilleur($idC,$valeur)
{
    $requete="INSERT into the_Bonus values (null,'%s', NOW(), '%s')";
    $query=sprintf($requete,$idC,$date,$valeur);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function updateMallusCueilleur($idC,$valeur)
{
    $requete="INSERT into the_Mallus values (null,'%s', NOW(), '%s')";
    $query=sprintf($requete,$idC,$date,$valeur);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function updateMallusGlobal($valeur) {
    $cueilleurs = getCueilleurs();
    foreach ($cueilleurs as $cueilleur) {
        updateMallusCueilleur($cueilleur['id'], $valeur);
    }
}

function updateBonusGlobal($valeur) {
    $cueilleurs = getCueilleurs();
    foreach ($cueilleurs as $cueilleur) {
        updateBonusCueilleur($cueilleur['id'], $valeur);
    }
}

function getPoidsCueillis($idParcelle,$datedeb,$dateFin) {
    $requete = sprintf("SELECT sum(poids) FROM v_PoidsRestants WHERE idParcelle = %d AND dateCueillette BETWEEN '%s' AND '%s'",
                        $idParcelle,
                        $dateDebut,
                        mysqli_real_escape_string(dbconnect(), $dateFin));
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    $poidsRestant = $row['poids_restant'];
    mysqli_free_result($resultat);
    return $poidsRestant;
}

function montantVente($dateDebut, $dateFin) {
    $requete = sprintf("SELECT sum(poids_prix)as total
                        FROM v_PoidsPrix
                        where dateCueillette BETWEEN '%s' AND '%s'",
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));

    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }

    $row = mysqli_fetch_assoc($resultat);
    $valiny= $row['total'];
    mysqli_free_result($resultat);
    return $valiny;
}


function getDernierMoisRegenerationAvantDate($date) {
    $requeteMoisRegeneration = sprintf("SELECT MAX(mois) AS dernier_mois_regeneration FROM the_Saisons WHERE is_regenere = 1 AND mois <= MONTH('%s')", mysqli_real_escape_string(dbconnect(), $date));
    $resultatMoisRegeneration = mysqli_query(dbconnect(), $requeteMoisRegeneration);
    if (!$resultatMoisRegeneration) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $rowMoisRegeneration = mysqli_fetch_assoc($resultatMoisRegeneration);
    $dernierMoisRegeneration = $rowMoisRegeneration['dernier_mois_regeneration'];
    mysqli_free_result($resultatMoisRegeneration);
    return $dernierMoisRegeneration;
}

function getMontantSalaireEtPoidsParRegeneration($dateDebut, $dateFin) {
    $dernierMoisRegeneration = getDernierMoisRegenerationAvantDate($dateDebut);
    $requete = sprintf("SELECT 
                            SUM(montant_salaire) AS montant_total,
                            SUM(poids) AS poids_total
                        FROM 
                            v_CueilletteSalaire c
                        JOIN
                            the_Saisons s ON MONTH(c.dateCueillette) = s.mois
                        WHERE 
                        MONTH(c.dateCueillette) <= '%d' 
                            AND c.dateCueillette BETWEEN '%s' AND '%s'",
                        $dernierMoisRegeneration,
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));

    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
    die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    mysqli_free_result($resultat);
    return $row;
}
function getMontantSalaireEtPoidsparCueilleur($idCueilleur,$dateDebut, $dateFin) {
    $requete = sprintf("SELECT 
                            v.*,
                            v.montant_salaire*v.poids_total as volaAzo
                        FROM 
                            v_CueilletteSalaire v
                        WHERE 
                            idCueilleur='%s'and
                            dateCueillette BETWEEN '%s' AND '%s'",$idCueilleur
                        mysqli_real_escape_string(dbconnect(), $dateDebut),
                        mysqli_real_escape_string(dbconnect(), $dateFin));

    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $row = mysqli_fetch_assoc($resultat);
    mysqli_free_result($resultat);
    return $row;
}

function getCueilleteBonus($idCueilleur)
{
        $requete = sprintf("SELECT * from v_CueilletteBonus where idCueilleur='%s");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $bonus=$resultat['valeur'];
        return $bonus;
}
function getCueilleteMallus($idCueilleur)
{
        $requete = sprintf("SELECT * from v_CueilletteMallus where idCueilleur='%s");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $mallus=$resultat['valeur'];
        return $mallus;
}

function getCueilletePoidsMin()
{
        $requete = sprintf("SELECT valeur from v_CueillettePoidsMin");
        $resultat = mysqli_query(dbconnect(), $requete);
        if (!$resultat) {
            die("Erreur dans la requête : " . mysqli_error(dbconnect()));
        }
        $poidsMin=$resultat['valeur'];
        return $poidsMin;
}

function AjustementSalaire($idCueilleur,$dateDebut, $dateFin)
{
    $poidsMin=floatval(getCueilletePoidsMin());
    $bonus=floatval(getCueilleteBonus($idCueilleur));
    $mallus=floatval(getCueilleteMallus($idCueilleur));
    $row=getMontantSalaireEtPoidsparCueilleur($idCueilleur,$dateDebut, $dateFin);
    $volaAzo=floatval( $row['volaAzo']);
    $poids= floatval($row['poids']);
        if ($poids>$poidsMin)
        {
            $volaAzo= $volaAzo+($volaAzo*$bonus/100);
        }
        if ($poids<$poidsMin)
        {
            $volaAzo= $volaAzo+($volaAzo*$mallus/100);
        }
        return $Volaazo;
}

function sommeSalaire ($dateDebut, $dateFin)
{   $result=0;
    $cueilleurs = getCueilleurs();
    foreach ($cueilleurs as $cueilleur) {
        $result+=AjustementSalaire($cueilleur['id'], $date, $valeur);
    }
    return $result;
}

function benefice ($dateDebut, $dateFin)
{   $montantVentes=montantVente($dateDebut, $dateFin);
    $depenses= getTotalDepensesPériode($dateDebut, $dateFin);
    $salaire=   sommeSalaire ($dateDebut, $dateFin);

    $benefice= $montantVentes-($depenses+$salaire);
    return $benefice;
}

//ampiana
function insertNewSaisons($mois, $is_regenere)
{
    $query = sprintf("INSERT INTO the_Saisons VALUES (null,'1','%s','%s')", $mois, $is_regenere);
    $result = mysqli_query(dbconnect(), $query);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getNombrePieds($idParcelle)
{
    $requete = sprintf("SELECT nbrePieds FROM v_nmbrePieds");
    $resultat = mysqli_query(dbconnect(), $requete);
    if (!$resultat) {
        die("Erreur dans la requête : " . mysqli_error(dbconnect()));
    }
    $total=$resultat['nbrePieds'];
    return $total;
}
