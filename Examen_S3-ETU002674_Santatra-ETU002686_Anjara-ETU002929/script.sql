CREATE TABLE the_Membres (
    id int auto_increment PRIMARY KEY, 
    nom VARCHAR(25),
    motdepasse VARCHAR(8),
    email VARCHAR(50),
    is_admin boolean
    );

CREATE TABLE the_Thes (
    id int auto_increment PRIMARY KEY, 
    nom VARCHAR(25),
    occupation decimal,
    rendement decimal
    );

CREATE TABLE the_Parcelles(
    id int auto_increment PRIMARY KEY,
    idThe int,
    surface decimal,
    FOREIGN KEY (idThe) REFERENCES the_Thes(id)
    );

CREATE TABLE the_Cueilleurs(
    id int auto_increment PRIMARY KEY,
    nom VARCHAR(25),
    genre char,
    dtn date
    );

CREATE TABLE the_CategorieDepenses(
    id int auto_increment PRIMARY KEY,
    nom VARCHAR(25)
    );

CREATE TABLE the_Depenses(
    id int auto_increment PRIMARY KEY,
    idCD int, 
    dateDepense date,
    montant decimal,
    idMembre int,
    FOREIGN KEY (idCD) REFERENCES the_CategorieDepenses(id),
    FOREIGN KEY (idMembre) REFERENCES the_Membres(id)
    );

create table the_Salaires(
    id int auto_increment PRIMARY KEY,
    dateSalaire date,
    montant decimal
);

CREATE TABLE the_Cueillettes(
    id int auto_increment PRIMARY KEY,
    dateCueillette date,
    idCueilleur int,
    idParcelle int,
    poids decimal, 
    FOREIGN KEY (idCueilleur) REFERENCES the_Cueilleurs(id),
    FOREIGN KEY (idParcelle) REFERENCES the_Parcelles(id)
    );
    
CREATE OR REPLACE VIEW v_PoidsRestants AS
SELECT 
    p.id AS idParcelle,
    c.dateCueillette,
    ((p.surface * 10000) * t.occupation * t.rendement) - IFNULL(SUM(c.poids), 0) AS poids_restant
FROM 
    the_Parcelles p
LEFT JOIN 
    the_Cueillettes c ON p.id = c.idParcelle
LEFT JOIN 
    the_Thes t ON p.idThe = t.id
GROUP BY
    p.id, c.dateCueillette;

CREATE VIEW v_CueilletteSalaire AS
SELECT c.*,
       (SELECT montant
        FROM the_Salaires s
        WHERE s.dateSalaire <= c.dateCueillette
        ORDER BY s.dateSalaire DESC
        LIMIT 1) AS montant_salaire
FROM the_Cueillettes c;
insert into the_Membres(nom, motdepasse, email, is_admin) values('admin', 'admin', 'admin', true);
insert into the_Membres(nom, motdepasse, email, is_admin) values('user', 'user', 'user',false);
select * from the_Thes;
select * from the_Cueilleurs;
select * from the_CategorieDepenses;

select* from the_Cueillettes;
select * from the_Depenses;
select * from the_Salaires;

-- Add missing import statement for the_Bonus table
CREATE OR REPLACE VIEW v_CueilletteBonus AS
SELECT c.*,b.valeur,b.dateBonus
FROM the_Cueillettes c
LEFT JOIN the_Bonus b ON b.dateBonus <= c.dateCueillette and c.idCueilleur=b.idCueilleur
ORDER BY b.dateBonus DESC
LIMIT 1;


CREATE VIEW v_CueilletteMallus AS
SELECT c.*, m.valeur,m.dateMallus
FROM the_Cueillettes c
LEFT JOIN the_Mallus m ON m.dateMallus <= c.dateCueillette and c.idCueilleur=m.idCueilleur
ORDER BY m.dateMallus DESC
LIMIT 1;

CREATE VIEW v_CueillettePoidsMin AS
SELECT c.*, pm.*
FROM the_Cueillettes c
LEFT JOIN the_PoidsMinimal Pm ON Pm.dateMin <= c.dateCueillette
ORDER BY pm.dateMin DESC
LIMIT 1;

CREATE VIEW v_PoidsPrix AS
SELECT 
    c.dateCueillette,
    c.poids,
    t.id,
    t.prix,
    c.poids * t.prix AS poids_prix
FROM 
    the_Parcelles p
LEFT JOIN 
    the_Cueillettes c ON p.id = c.idParcelle
LEFT JOIN 
    the_Thes t ON p.idThe = t.id
GROUP BY
    t.id, c.dateCueillette;


ALTER TABLE the_Thes
ADD prix decimal;


CREATE TABLE the_Saisons(
    id int auto_increment PRIMARY KEY,
    mois int,
    is_regenere int CHECK (is_regenere IN (0, 1))
);

CREATE TABLE the_PoidsMinimal (
    id int auto_increment PRIMARY KEY,
    valeur decimal,
    dateMin date
);

CREATE TABLE the_Bonus(
    id int auto_increment PRIMARY KEY,
    idCueilleur int,
    dateBonus date,
    valeur decimal,
    FOREIGN KEY (idCueilleur) REFERENCES the_Cueilleurs(id)
);

CREATE TABLE the_Mallus (
    id int auto_increment PRIMARY KEY,
    idCueilleur int,
    dateMallus date,
    valeur decimal,
    FOREIGN KEY (idCueilleur) REFERENCES the_Cueilleurs(id)
);

//ampiana

CREATE OR REPLACE VIEW v_nmbrePieds AS
SELECT 
    p.id AS idParcelle,
    ((p.surface * 10000) / t.occupation) AS nbrePieds
FROM 
    the_Parcelles p
LEFT JOIN 
    the_Cueillettes c ON p.id = c.idParcelle
LEFT JOIN 
    the_Thes t ON p.idThe = t.id
GROUP BY
    p.id, c.dateCueillette;
