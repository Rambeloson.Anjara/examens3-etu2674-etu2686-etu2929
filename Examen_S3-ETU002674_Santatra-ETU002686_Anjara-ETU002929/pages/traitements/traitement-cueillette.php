<?php
include("../../inc/fonction.php");

// Set up error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Check if the form fields are set
if (isset($_POST['date-cueillette']) && isset($_POST['cueilleur']) && isset($_POST['parcelle']) && isset($_POST['poids'])) {
    $dateCueillette = $_POST['date-cueillette'];
    $idCueilleur = $_POST['cueilleur'];
    $idParcelle = $_POST['parcelle'];
    $poids = $_POST['poids'];

    // Check if the weight is valid
    if (checkPoidsRestant($idParcelle, $poids)) {
        // Weight is valid, proceed with insertion
        $result = insertNewCueillette($dateCueillette, $idCueilleur, $idParcelle, $poids);

        if ($result) {
            // Return a success response
            echo json_encode(['success' => true]);
        } else {
            // Return an error response
            echo json_encode(['error' => 'Erreur lors de l\'enregistrement']);
        }
    } else {
        // Weight is invalid, return an error response
        echo json_encode(['error' => 'Invalid weight']);
    }
} else {
    // Fields are missing, return an error response
    echo json_encode(['error' => 'Tous les champs sont obligatoires']);
}
?>