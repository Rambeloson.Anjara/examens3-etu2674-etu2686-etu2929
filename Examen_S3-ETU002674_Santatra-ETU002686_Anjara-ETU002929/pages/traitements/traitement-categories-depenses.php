<?php
include ("../../inc/fonction.php");
if(isset($_POST['categorie']))
{
    $nom = $_POST['categorie'];
    $result = insertNewCategorieDepenses($nom);
    if($result)
    {
        header('Location: ../pages/admin-categories-depenses.php');
    }
    else
    {
        echo "Erreur";
    }
}
else
{
    echo "Erreur : Il manque des informations.";
}
?>