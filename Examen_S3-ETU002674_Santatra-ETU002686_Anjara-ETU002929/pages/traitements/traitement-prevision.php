<?php
include("../../inc/fonction.php");

// Set up error reporting for debugging
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Check if the form fields are set
if (isset($_POST['date'])) {
    $date= $_POST['date'];
    $listesParcelles=getParcelles();
    $ventes=montantVente('2023-01-01',$date);
    $poidsTotalRestant=getPoidsRestantParcelleGlobal($date);
    if($date !=null && $listesParcelles!=null && $ventes!=null && $poidsTotalRestant!=null)
    {
        // Return a success response
        echo json_encode(['success' => true,'ventes'=>$ventes,'poidsTotalRestant'=>$poidsTotalRestant,'listesParcelles'=>$listesParcelles]);
    } else {
        // Return an error response
        echo json_encode(['error' => 'Erreur lors de l\'enregistrement']);
    }
} else {
    // Fields are missing, return an error response
    echo json_encode(['error' => 'Tous les champs sont obligatoires']);
}
?>