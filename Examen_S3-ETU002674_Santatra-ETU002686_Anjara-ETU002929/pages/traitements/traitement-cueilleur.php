<?php
include ("../../inc/fonction.php");
if(isset($_POST['nom']) && isset($_POST['sexe']) && isset($_POST['date-naissance']))
{
    $nom = $_POST['nom'];
    $sexe = $_POST['sexe'];
    $date_naissance = $_POST['date-naissance'];
    $result = insertNewCueilleurs($nom,$sexe,$date_naissance);
    if($result)
    {
        header('Location: ../pages/admin-cueilleurs.php');
    }
    else
    {
        echo "Erreur";
    }
}
else if(isset($_POST['salaire']))
{
    $salaire = $_POST['salaire'];
    $result=updateSalaire($salaire);
    if($result)
    {
        header('Location: ../pages/admin-cueilleurs.php');
    }
    else
    {
        echo "Erreur";
    }
    
}
else if(isset($_POST['minimum']))
{
    $valeur=$_POST['minimum'];
    updatePoidsMinimal($valeur)
}
else if(isset($_POST['bonus']))
{
    $valeur=$_POST['bonus'];
    updateBonusGlobal($valeur);
}
else if(isset($_POST['malus']))
{
    $valeur=$_POST['malus'];
    updateMallusGlobal($valeur);
}
else
{
    echo "Erreur : Il manque des informations.";
}
?>