<?php
include ("../../inc/fonction.php");
$login = $_POST['username'];
$password = $_POST['password'];
session_start();
$user = checkConnexion($login,$password);
if($user!=null)
{
    $_SESSION['id'] = $user['id'];
    $_SESSION['is_admin'] = check_admin($user['id']);
    if($_SESSION['is_admin'])
    {
        header('Location: ../pages/admin-variete.php');
    }
    else
    {
        header('Location: ../pages/home.php');
    }
}
else
{
    header('Location: ../pages/index.php');
}
?>