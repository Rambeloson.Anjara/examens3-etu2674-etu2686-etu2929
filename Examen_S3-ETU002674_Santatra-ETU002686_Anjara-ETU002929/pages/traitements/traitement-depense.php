<?php
include ("../../inc/fonction.php");
session_start();
if(isset($_POST['date-depense']) && isset($_POST['categorie']) && isset($_POST['montant']) && isset($_SESSION['id']))
{
    if(insertNewDepenses($_POST['categorie'],$_POST['date-depense'],$_POST['montant'],$_SESSION['id']))
    {
        header('Location: ../pages/home.php');
    }
    else
    {
        echo "Erreur";
    }
}
else
{
    echo "Erreur: Tous les champs sont obligatoires";
}
?>