<?php
include('../../inc/fonction.php');
session_start();
if(isset($_POST['date-debut']) && isset($_POST['date-fin']))
{
    $dateDebut=$_POST['date-debut'];
    $dateFin=$_POST['date-fin'];
    $timestampDebut = strtotime($dateDebut);
    $timestampFin = strtotime($dateFin);

    if($timestampDebut > $timestampFin) {
        $dateFin=$_POST['date-debut'];
        $dateDebut=$_POST['date-fin'];
    }
    $poidsTotalCueillette=getPoidsTotalCueilletteParPeriode($dateDebut, $dateFin);
    $poidsRestantsSurLesParcelles=getPoidsRestantParcelleGlobal($dateFin);
    $coutRevientKg=calculRevientParKiloParPeriode($dateDebut, $dateFin);
    $totalDepenses=getTotalDepensesPeriode($dateDebut, $dateFin);
    $result = array(
        "poidsTotalCueillette" => $poidsTotalCueillette,
        "poidsRestantsSurLesParcelles" => $poidsRestantsSurLesParcelles,
        "coutRevientKg" => $coutRevientKg,
        "totalDepenses" => $totalDepenses,
    );
    $_SESSION['benefice']=benefice($dateDebut, $dateFin);
    $_SESSION['vente']=montantVente($dateDebut, $dateFin);
    $_SESSION['resultat']=$result;
    header('Location: ../pages/resultat.php');
}
else
{
    echo "no set";

}
?>
