<?php
include("../../inc/fonction.php");
$listesParcelles=getParcelles();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>production et cueillette de thé</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="../../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../../assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="../../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="../../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/style2.css" rel="stylesheet">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $("#formCueillette").submit(function (event) {
          console.log("Form submitted");
            event.preventDefault();
            event.preventDefault();
            var formData = $(this).serialize();
            console.log(formData);

            // Send an AJAX request
            $.ajax({
                type: "POST",
                url: "../traitements/traitement-prevision.php",
                data: formData,
                dataType: "json",
                success: function (response) {
                  $("#therestant").text("Poids de thé restant : "+response.poidsTotalRestant);
                  $("#montant").text("Montant : "+response.ventes);
                  
                    if (response.error) {
                        
                        $("#error-message").text(response.error).show();
                    } else {
                        // Redirect on success
                        window.location.href = "prevision.php";
                    }
                    
                },
                error: function (xhr, status, error) {
                    // Handle AJAX error
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <h1 class="logo me-auto me-lg-0">thé<span>.</span></h1>

    <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
            <li><a class="nav-link scrollto active" href="index.php">Quitter</a></li>
            <li><a class="nav-link scrollto active" href="home.php">Home</a></li>
            <li><a class="nav-link scrollto active" href="resultat.php">Resultat</a></li>
            <li><a class="nav-link scrollto active" href="paiement-salaires.php">Paiement salaires</a></li>
            <li><a class="nav-link scrollto active" href="paiement-salaires.php">Prevision</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
      
  </header><!-- End Header -->

  <main id="main">

  <section id="team features" class="team features">
        <div class="container" data-aos="fade-up" data-aos="zoom-in" data-aos-delay="100">

        <div class="section-title">
          <h2></h2>
          <p>Prevision</p>
        </div>

        <div class="row">
          <div class="image col-lg-6" style='background-image: url("assets/img/features.jpg");' data-aos="fade-right">
                        <form id="formCueillette" class="row g-3 needs-validation" novalidate method="POST" action="../traitements/traitement-cueillette.php">
                          <div class="col-12">
                          <label for="yourUsername" class="form-label">date</label>
                          <div class="input-group has-validation">
                              <input type="date" class="form-control" name="date" required>
                          </div>
                          </div>

                          <div class="col-12">
                          <button class="btn btn-primary w-100" type="submit">Valider</button>
                          </div>
                      </form>
          </div>
          <div class="col-lg-6" data-aos="fade-left" data-aos-delay="100">
            <div class="icon-box mt-5 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
              <i class="bx bx-receipt"></i>
              <h4>Poids de thé restant</h4>
              <p id="therestant"></p>
            </div>
            <div class="icon-box mt-5 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
              <i class="bx bx-receipt"></i>
              <h4>Montant</h4>
              <p id="montant"></p>
            </div>
          </div>
        </div>

        <div class="row">

        <?php foreach($listesParcelles as $parcelle) { ?>
          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="../../assets/img/thee.png" class="img-fluid" alt="">
                <div class="social">
                  <h4 style="color:#f6f9ff"><b><?php echo $parcelle['id'] ;?></b></h4>
                  <h5 style="color:#f6f9ff"><b><?php echo $parcelle['surface'] ;?></b></h5>
                </div>
              </div>
              <div class="member-info">
                <h4><?php echo $parcelle['nom'] ;?></h4>
                <span><?php echo getNombrePieds($parcelle['id']);?></span>
                <span id="poidsRestant"></span>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>

      </div>
    </section>
  </main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright 
      </br><strong><span>Shamsia</span></strong>. ETU002929
      </br><strong><span>Anjara</span></strong>. ETU002686
      </br><strong><span>Santatra</span></strong>. ETU002674
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="../../assets/vendor/aos/aos.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main2.js"></script>

</body>

</html>