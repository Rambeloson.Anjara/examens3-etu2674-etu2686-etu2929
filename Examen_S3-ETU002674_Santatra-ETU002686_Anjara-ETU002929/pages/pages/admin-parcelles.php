<?php
include ("../../inc/fonction.php");
$listeThes=getVarietesThes();
$listeThes = json_decode($listeThes, true);
$listeParcelle=getParcelles();
?><!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>production et cueillette de thé</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="../../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../../assets/vendor/simple-datatables/style.css" rel="stylesheet">


  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  

  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/table.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
      <a href="index.php" class="logo d-flex align-items-center">
        <span class="d-none d-lg-block">Page Admin</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div>
  </header><!-- End Header -->

        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">

            <ul class="sidebar-nav" id="sidebar-nav">

            <li class="nav-heading">Pages parcelle</li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-variete.php">
                    <span>Varieté</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-cueilleurs.php">
                    <span>Cueilleurs</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-categories-depenses.php">
                    <span>catégories de dépenses</span>
                    </a>
                </li>
            </ul>

        </aside><!-- End Sidebar-->

        <main id="main" class="main">
            <div class="pagetitle">
                <h1>Gestion de parcelles</h1>
            </div>
            
            <section class="section dashboard">
              <div class="row">

                <!-- Left side columns -->
                <div class="col-lg-4">
                  <div class="row">
                    <div class="col-12">
                      <div class="card top-selling overflow-auto">
                        <div class="filter">
                          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                        </div>
                        <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">Ajout parcelles</h5>
                            <p class="text-center small"></p>
                        </div>

                                <!-- formulaire de login -->
                                <form class="row g-3 needs-validation" novalidate method="post" action="../traitements/traitement-parcelle.php">
                                    <div class="col-12">
                                    <label class="form-label">surface en hectare</label>
                                    <div class="input-group has-validation">
                                        <!-- input pour l'occupation -->
                                        <input type="number" class="form-control" name="surface" required>
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <label for="inputState" class="form-label">variété de thé planté</label>
                                    <div class="form-floating mb-3">
                                    <select id="inputState" class="form-select" name="the">
                                        <option selected>Choisisez</option>
                                        <option>
                                        <?php 
                                        foreach($listeThes as $the) {
                                            echo "<option value='".$the['id']."'>".$the['nom']."</option>";
                                        }
                                        ?>
                                        </option>
                                    </select>
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <button class="btn btn-primary w-100" type="submit">Ajouter</button>
                                    </div>
                                </form>

                        </div>
                      </div>
                    </div><!-- End Top Selling Medicament-->

                  </div>
                </div><!-- End Left side columns -->

                <!-- Right side columns -->
                <div class="col-lg-8">
                    <div class="row">
                    <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                        <h5 class="card-title">Liste des variétés de thés et rendements</h5>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Surface en hectare</th>
                                    <th>variété de thé planté</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach($listeParcelle as $parcelle){?>
                                <tr>
                                    <td><?php echo $parcelle['surface']; ?></td>
                                    <td><?php echo $parcelle['nom']; ?></td>
                                    <td>
                                    <a href="../traitements/traitement-update.php? table='the_Parcelles' && id=<?php echo $parcelle['id']; ?>
                                    && surface=<?php echo $parcelle['surface']; ?> && variete=<?php echo $parcelle['nom']; ?>"
                                     class="add" title="Add" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a href="../traitements/traitement-delete.php? table='the_Parcelles' && id=<?php echo $parcelle['id']; ?>"
                                        class="delete" title="Delete" data-toggle="tooltip">
                                          <i class="material-icons"></i></a>
                                    </td>
                                </tr>  
                                <?php }?>   
                            </tbody>
                        </table>
                        </div>
                  </div>

                </div>
              </div>
                </div>
              </div>
            </section>
          </main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright 
      </br><strong><span>Shamsia</span></strong>. ETU002929
      </br><strong><span>Anjara</span></strong>. ETU002686
      </br><strong><span>Santatra</span></strong>. ETU002674
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/chart.js/chart.umd.js"></script>
  <script src="../../assets/vendor/echarts/echarts.min.js"></script>
  <script src="../../assets/vendor/quill/quill.min.js"></script>
  <script src="../../assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="../../assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main.js"></script>
  <script src="../js/table_parcelles.js"></script>

</body>

</html>