<?php 
include ("../../inc/fonction.php");
$listeCueilleurs=getCueilleurs();
$listeParcelles=getParcelles(); 
$listeCategoriesDepenses=getCategorieDepenses();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>production et cueillette de thé</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans" rel="stylesheet">

  <!-- Bootstrap CSS (Choose one version) -->
  <!-- Bootstrap 4 -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
  <!-- OR Bootstrap 3 -->
  <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Your custom stylesheets -->
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="../../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../../assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="../../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="../../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Your custom styles -->
  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/style2.css" rel="stylesheet">

  <!-- jQuery (Choose one version) -->
  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
  <!-- OR use the jQuery provided by Bootstrap -->
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->

  <!-- Bootstrap JS (Choose one version) -->
  <!-- Bootstrap 4 -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        // Attach an event handler to the form submission
        $("#formCueillette").submit(function (event) {
          console.log("Form submitted");
            event.preventDefault();
            event.preventDefault();
            // Get form data
            var formData = $(this).serialize();
            console.log(formData);

            // Send an AJAX request
            $.ajax({
                type: "POST",
                url: "../traitements/traitement-cueillette.php",
                data: formData,
                dataType: "json",
                success: function (response) {
                    if (response.error) {
                        // Display the error message
                        $("#error-message").text(response.error).show();
                    } else {
                        // Redirect on success
                        window.location.href = "home.php";
                    }
                },
                error: function (xhr, status, error) {
                    // Handle AJAX error
                    console.error(xhr.responseText);
                }
            });
        });
    });
</script>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <h1 class="logo me-auto me-lg-0">thé<span>.</span></h1>

    <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="index.php">Quitter</a></li>
          <li><a class="nav-link scrollto active" href="">Home</a></li>
          <li><a class="nav-link scrollto active" href="resultat.php">Resultat</a></li>
          <li><a class="nav-link scrollto active" href="paiement-salaires.php">Paiement salaires</a></li>
          <li><a class="nav-link scrollto active" href="paiement-salaires.php">Prevision</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-6 col-lg-8">
          <h1>Bonjour à vous<span>.</span></h1>
        </div>
      </div>

      <div class="row gy-4 mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">


  <!-- admin-variete -->
  <section id="about" class="about">
      <div class="container" data-aos="fade-up" data-aos="zoom-in" data-aos-delay="100">
      <div class="section-title">
          <h2>Saisie</h2>
          <p>Saisie cueillette</p>
        </div>

        <div class="row">
          <div class="col-5">
            <div class="card top-selling overflow-auto">
              <div class="card-body">

              <div class="pt-4 pb-2">
                  <h5 class="card-title text-center pb-0 fs-4">remplir</h5>
                  <p class="text-center small"></p>
              </div>
                      <form id="formCueillette" class="row g-3 needs-validation" novalidate method="POST" action="../traitements/traitement-cueillette.php">
                          <div class="col-12">
                          <label for="yourUsername" class="form-label">date</label>
                          <div class="input-group has-validation">
                              <input type="date" class="form-control" name="date-cueillette" required>
                          </div>
                          </div>

                          <div class="col-12">
                          <label for="inputState" class="form-label">choix cueilleur</label>
                          <div class="form-floating mb-3">
                          <select id="inputState" class="form-select" name="cueilleur">
                              <option selected>Choisisez</option>
                              <?php foreach($listeCueilleurs as $cueilleur) { ?>
                                  <option value="<?php echo $cueilleur['id']; ?>"><?php echo $cueilleur['nom']; ?></option>
                              <?php } ?>
                          </select>
                          </div>
                          </div>

                          <div class="col-12">
                          <label for="inputState" class="form-label">choix parcelle</label>
                          <div class="form-floating mb-3">
                          <select id="inputState" class="form-select" name="parcelle">
                              <option selected>Choisisez</option>
                              <?php foreach($listeParcelles as $parcelle) { ?>
                                  <option value="<?php echo $parcelle['id']; ?>"><?php echo $parcelle['id']; ?></option>
                              <?php } ?>
                          </select>
                          </div>
                          </div>

                          <div class="col-12">
                              <label for="yourUsername" class="form-label">poids cueilli</label>
                              <div class="input-group has-validation">
                                  <input type="number" class="form-control" name="poids">
                              </div>
                              <span id="error-message" style="color: red; display: none;"></span>
                          </div>
                          </div>

                          <div class="col-12">
                          <button class="btn btn-primary w-100" type="submit">Valider</button>
                          </div>
                      </form>
              </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>saisie</h2>
          <p>Saisie dépenses</p>
        </div>

        <div class="row">
        <div class="col-5">
            <div class="card top-selling overflow-auto">
              <div class="card-body">

              <div class="pt-4 pb-2">
                  <h5 class="card-title text-center pb-0 fs-4">remplir</h5>
                  <p class="text-center small"></p>
              </div>
                      <form class="row g-3 needs-validation" method="post" action="../traitements/traitement-depense.php" novalidate>
                          <div class="col-12">
                          <label for="yourUsername" class="form-label">date</label>
                          <div class="input-group has-validation">
                              <input type="date" class="form-control" name="date-depense" required>
                          </div>
                          </div>

                          <div class="col-12">
                          <label for="inputState" class="form-label">choix catégorie dépenses</label>
                          <div class="form-floating mb-3">
                          <select id="inputState" class="form-select" name="categorie" >
                              <option selected>Choisisez</option>
                              <?php foreach($listeCategoriesDepenses as $categorieDepense) { ?>
                                  <option value="<?php echo $categorieDepense['id']; ?>"><?php echo $categorieDepense['nom']; ?></option>
                              <?php } ?>
                          </select>
                          </div>
                          </div>

                          <div class="col-12">
                          <label for="yourUsername" class="form-label">montant</label>
                          <div class="input-group has-validation">
                              <input type="number" class="form-control" name="montant" required>
                          </div>
                          </div>

                          <div class="col-12">
                          <button class="btn btn-primary w-100" type="submit">Valider</button>
                          </div>
                      </form>
              </div>
          </div>
        </div>
      </div>
    </section><!-- End Services Section -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright 
      </br><strong><span>Shamsia</span></strong>. ETU002929
      </br><strong><span>Anjara</span></strong>. ETU002686
      </br><strong><span>Santatra</span></strong>. ETU002674
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="../../assets/vendor/aos/aos.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main2.js"></script>

</body>

</html>