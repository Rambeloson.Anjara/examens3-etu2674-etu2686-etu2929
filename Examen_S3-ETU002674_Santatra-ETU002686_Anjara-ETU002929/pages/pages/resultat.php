<?php
include('../../inc/fonction.php');
session_start();
if(isset($_SESSION['resultat']))
{
    $result = $_SESSION['resultat'];
    $valeurs = $result;
}
else
{
    $valeurs = array(
        "poidsTotalCueillette" => 1,
        "poidsRestantsSurLesParcelles" => 0,
        "coutRevientKg" => 0,
        "totalDepenses" => 0,
    );
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>production et cueillette de thé</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="../../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../../assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="../../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="../../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/style2.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <h1 class="logo me-auto me-lg-0">thé<span>.</span></h1>
    <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
            <li><a class="nav-link scrollto active" href="index.php">Quitter</a></li>
            <li><a class="nav-link scrollto active" href="home.php">Home</a></li>
            <li><a class="nav-link scrollto active" href="resultat.php">Resultat</a></li>
            <li><a class="nav-link scrollto active" href="paiement-salaires.php">Paiement salaires</a></li>
            <li><a class="nav-link scrollto active" href="paiement-salaires.php">Prevision</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
        <i><form class="row g-3 needs-validation" method="post" action="../traitements/traitement-resultat.php">
          <div class="col-md-4">
              <label for="date-debut" class="form-label">Début</label>
              <input type="date" class="form-control" id="date-debut" name="date-debut" required>
          </div>
          <div class="col-md-4">
              <label for="date-fin" class="form-label">Fin</label>
              <input type="date" class="form-control" id="date-fin" name="date-fin" required>
          </div>
          <div class="col-md-4">
          <label for="date-fin" class="form-label">.</label>
          <button class="btn btn-primary w-100" type="submit">Afficher</button>
          </div>
              </form></i>
      </nav><!-- .navbar -->
  </header><!-- End Header -->
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-6 col-lg-8">
          <h1>Bonjour à vous<span>.</span></h1>
        </div>
      </div>

      <div class="row gy-4 mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
        <div class="col-xl-2 col-md-4">
          <div class="icon-box">
            <i class="ri-store-line"></i>
            <h3><a href="">poids total cueillette</a></h3>
            <h6><?php echo $valeurs['poidsTotalCueillette'];?></h6>
          </div>
        </div>
        <div class="col-xl-2 col-md-4">
          <div class="icon-box">
            <i class="ri-bar-chart-box-line"></i>
            <h3><a href="">poids restants sur les parcelles</a></h3>
            <h6><?php echo $valeurs['poidsRestantsSurLesParcelles'];?></h6>
          </div>
        </div>
        <div class="col-xl-2 col-md-4">
          <div class="icon-box">
            <i class="ri-calendar-todo-line"></i>
            <h3><a href="">cout de revient/kg</a></h3>
            <h6><?php echo $valeurs['coutRevientKg'];?></h6>
          </div>
        </div>
        <div class="col-xl-2 col-md-4">
          <div class="icon-box">
            <i class="ri-calendar-todo-line"></i>
            <h3><a href="">Total depenses</a></h3>
            <h6><?php echo $valeurs['totalDepenses']; ?></h6>
          </div>
        </div>

    </div>
  </section><!-- End Hero -->

  <!-- ITO NY VAOVAO -->
  <section id="counts" class="counts">
      <div class="container" data-aos="fade-up">

        <div class="row no-gutters">
          <div class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start" data-aos="fade-right" data-aos-delay="100"></div>
          <div class="col-xl-7 ps-4 ps-lg-5 pe-4 pe-lg-1 d-flex align-items-stretch" data-aos="fade-left" data-aos-delay="100">
            <div class="content d-flex flex-column justify-content-center">
              <h3>Global</h3>
              <div class="row">
                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="bi bi-journal-richtext"></i>
                    <span data-purecounter-start="0" data-purecounter-end="<?php echo $valeurs['poidsRestantsSurLesParcelles'];?>" data-purecounter-duration="2" class="purecounter"></span>
                    <p><strong>Poids</strong> restant sur les parcelles</p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="bi bi-journal-richtext"></i>
                    <span data-purecounter-start="0" data-purecounter-end="<?php echo $_SESSION['vente'];?>" data-purecounter-duration="2" class="purecounter"></span>
                    <p><strong>Montant</strong> des ventes </p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="bi bi-award"></i>
                    <span data-purecounter-start="0" data-purecounter-end="<?php echo $valeurs['totalDepenses']; ?>" data-purecounter-duration="4" class="purecounter"></span>
                    <p><strong>Montant</strong> des dépenses</p>
                  </div>
                </div>

                <div class="col-md-6 d-md-flex align-items-md-stretch">
                  <div class="count-box">
                    <i class="bi bi-award"></i>
                    <span data-purecounter-start="0" data-purecounter-end="</strong><?php echo $_SESSION['benefice'];?>" data-purecounter-duration="4" class="purecounter"></span>
                    <p><strong>Bénéfice </p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End Counts Section -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright 
      </br><strong><span>Shamsia</span></strong>. ETU002929
      </br><strong><span>Anjara</span></strong>. ETU002686
      </br><strong><span>Santatra</span></strong>. ETU002674
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="../../assets/vendor/aos/aos.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main2.js"></script>

</body>

</html>