<?php
include ("../../inc/fonction.php");
$listeThes=getVarietesThes();
$listeThes = json_decode($listeThes, true);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>production et cueillette de thé</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="../../assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="../../assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="../../assets/vendor/simple-datatables/style.css" rel="stylesheet">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/table.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
      <a href="index.php" class="logo d-flex align-items-center">
        <span class="d-none d-lg-block">Page Admin</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div>
  </header><!-- End Header -->

        <!-- ======= Sidebar ======= -->
        <aside id="sidebar" class="sidebar">

            <ul class="sidebar-nav" id="sidebar-nav">

            <li class="nav-heading">Pages gestion</li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-parcelles.php">
                    <span>Parcelles</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-cueilleurs.php">
                    <span>Cueilleurs</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="admin-categories-depenses.php">
                    <span>catégories de dépenses</span>
                    </a>
                </li>
            </ul>

            <!-- ITO NY VAOVAO -->
            <ul class="sidebar-nav" id="sidebar-nav">
              <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="#">
                  <i class="bi bi-journal-text"></i><span>Config mois</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <section class="section register min-vh-40 d-flex flex-column align-items-center justify-content-center py-4">
                    <form class="row g-3 needs-validation" novalidate method="post" action="../traitements/traitement-variete.php">
                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="01">
                          <label class="form-check-label" for="gridCheck1">
                            Janvier
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="02">
                          <label class="form-check-label" for="gridCheck1">
                            Fevrier
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="03">
                          <label class="form-check-label" for="gridCheck1">
                            Mars
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="04">
                          <label class="form-check-label" for="gridCheck1">
                            Avril
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="05">
                          <label class="form-check-label" for="gridCheck1">
                            Mai
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="06">
                          <label class="form-check-label" for="gridCheck1">
                            Juin
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="07">
                          <label class="form-check-label" for="gridCheck1">
                            Juillet
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="08">
                          <label class="form-check-label" for="gridCheck1">
                            Aout
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="09">
                          <label class="form-check-label" for="gridCheck1">
                            Septembre
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1"  name="10">
                          <label class="form-check-label" for="gridCheck1">
                            Octobre
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="11">
                          <label class="form-check-label" for="gridCheck1">
                            Novembre
                          </label>
                        </div>
                        </div>

                        <div class="col-12">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck1" name="12">
                          <label class="form-check-label" for="gridCheck1">
                            Decembre
                          </label>
                        </div>
                        </div>
                        <div class="col-12">
                          <button class="btn btn-primary w-100" type="submit">Valider</button>
                        </div>
                    </form>
                  </section>
                </ul>
              </li>
            </ul>

        </aside><!-- End Sidebar-->

        <main id="main" class="main">
            <div class="pagetitle">
                <h1>Gestion de varietés</h1>
            </div>
            
            <section class="section dashboard">
              <div class="row">

                <!-- Left side columns -->
                <div class="col-lg-4">
                  <div class="row">
                    <div class="col-12">
                      <div class="card top-selling overflow-auto">
                        <div class="filter">
                          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                        </div>
                        <div class="card-body">

                        <div class="pt-4 pb-2">
                            <h5 class="card-title text-center pb-0 fs-4">Ajout varieté</h5>
                            <p class="text-center small"></p>
                        </div>

                                <form class="row g-3 needs-validation" novalidate method="post" action="../traitements/traitement-variete.php">
                                    <div class="col-12">
                                    <label for="yourUsername" class="form-label">Nom</label>
                                    <div class="input-group has-validation">
                                        <!-- input pour le nom -->
                                        <input type="text" class="form-control" name="nom" id="nom" required>
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <label for="yourUsername" class="form-label">Occupation en m2</label>
                                    <div class="input-group has-validation">
                                        <!-- input pour l'occupation -->
                                        <input type="number" class="form-control" name="occupation" id="occupation" required>
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <label for="yourUsername" class="form-label">Rendement (en kilo par pied)</label>
                                    <div class="input-group has-validation">
                                        <!-- input pour le nom -->
                                        <input type="number" class="form-control" name="rendement" id="rendement" required>
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <label for="yourUsername" class="form-label">Prix</label>
                                    <div class="input-group has-validation">
                                        <input type="number" class="form-control" required name="prix">
                                    </div>
                                    </div>

                                    <div class="col-12">
                                    <button class="btn btn-primary w-100" type="submit">Ajouter</button>
                                    </div>
                                </form>
                        </div>
                      </div>
                    </div><!-- End Top Selling Medicament-->

                  </div>
                </div><!-- End Left side columns -->

                <!-- Right side columns -->
                <div class="col-lg-8">
                  <div class="card">
                      <div class="card-body">
                        <h5 class="card-title">Liste des variétés de thés et rendements</h5>
                        <table class="table table-bordered">
                          <thead>
                              <tr>
                                  <th>Nom</th>
                                  <th>Occupation en m2</th>
                                  <th>Rendement</th>
                                  <th>Prix</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                  <?php foreach($listeThes as $thes) { ?>
                                  <tr>
                                  <td><?php echo $thes['nom']; ?></td>
                                  <td><?php echo $thes['occupation']; ?></td>
                                  <td><?php echo $thes['rendement']; ?></td>
                                  <td><?php echo $thes['prix']; ?></td>
                                  <td>
                                    <a href="../traitements/traitement-update.php? table='the_Thes' && id=<?php echo $thes['id']; ?> 
                                    && nom=<?php echo $thes['nom']; ?> && occupation=<?php echo $thes['occupation']; ?> && rendement=<?php echo $thes['rendement']; ?>"
                                     class="add" title="Add" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a href="../traitements/traitement-delete.php? table='the_Thes' && id=<?php echo $thes['id']; ?>"
                                        class="delete" title="Delete" data-toggle="tooltip">
                                          <i class="material-icons"></i></a>
                                    </td>
                                  </tr> 
                                  <?php } ?>
                                
                          </tbody>
                      </table>
                      </div>
                    </div>
                </div>
              </div>
            </section>
          </main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright 
      </br><strong><span>Shamsia</span></strong>. ETU002929
      </br><strong><span>Anjara</span></strong>. ETU002686
      </br><strong><span>Santatra</span></strong>. ETU002674
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/chart.js/chart.umd.js"></script>
  <script src="../../assets/vendor/echarts/echarts.min.js"></script>
  <script src="../../assets/vendor/quill/quill.min.js"></script>
  <script src="../../assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="../../assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main.js"></script>
  <script src="../js/table_variete.js"></script>

</body>

</html>